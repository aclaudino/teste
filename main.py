#!/usr/bin/env python

import dash
from dash.dependencies import Input, Output, State
import pandas as pd
import urllib

import apps.front_page as front
from apps.jobs import layout as jobs
from apps.plataformas import layout as plataformas
from apps.empresas import layout as empresas
from apps.contratos import layout as contratos

import apps.plataformas.callback as plataformas_c
import apps.jobs.callback as jobs_c

from apps.utils import desunderline
from persistense.load_config import LoadConfig

application = dash.Dash("Synapse Job Service")
application.config.supress_callback_exceptions = True
application.css.append_css({'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'})
application.css.append_css({'external_url': 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'})

application.layout = front.layout


# Multipage
@application.callback(Output('page-content', 'children'), [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/' or pathname == '/jobs':
        return jobs.layout
    if pathname == '/plataformas':
        return plataformas.layout
    if pathname == '/empresas':
        return empresas.layout
    if pathname == '/contratos':
        return contratos.layout
    else:
        return jobs.layout


@application.callback(Output('tab-plataforma-content', 'children'),
                      [Input('tab-plataforma', 'value')])
def manage_tabs_plataformas(value):
    if value == 'pracas':
        return plataformas.pracas_layout
    if value == 'janelas':
        return plataformas.janelas_layout


@application.callback(Output('input-contrato-plataformas', 'rows'),
                      [Input('contracts-config-table', 'rows'),
               Input('contracts-config-table', 'selected_row_indices')])
def list_current_platforms_for_contract(rows, indexes):
    if not indexes:
        return [{}]

    output = []
    for index in indexes:
        codigo = rows[index]["CODIGO DE PROGRAMA"]
        plataformas = LoadConfig()\
            .contracts\
            .find_one({"CODIGO DE PROGRAMA": codigo})["PLATAFORMAS"]

        for plataforma in plataformas:
            plataforma["CODIGO DE PROGRAMA"] = codigo

            output.append(desunderline(plataforma))
        return output



# === Callback de plataformas ===========
# Carrega janelas a partir da plataforma selecionada
@application.callback(Output('janelas-config-table', 'rows'),
                      [Input('selected-platform', 'value')])
def load_janelas(seleted_platform):
    return plataformas_c.load_janelas(seleted_platform)

# Carrega praças a partir da plataforma selecionada
@application.callback(Output('plataformas-config-table', 'rows'),
                      [Input('selected-platform', 'value')])
def load_pracas(seleted_platform):
    return plataformas_c.load_pracas(seleted_platform)

# Insere uma nova praça
@application.callback(Output('selected-platform', 'value'),
                      [Input('praca-inserir-button', 'n_clicks')],
                      [State('selected-platform', 'value'),
               State('praca-input', 'value'),
               State('taxa-imposto-input', 'value'),
               State('plataformas-config-table', 'rows')])
def insert_praca(_, plataforma, nome, taxa, rows):
    return plataformas_c.insert_praca(plataforma, nome, taxa, rows)


# ======= executa job ==========
@application.callback(Output('job_table', 'rows'),
                      [Input('executar', 'n_clicks')],
                      [State('uploader', 'contents'),
               State('plataforma', 'value'),
               State('empresa', 'value'),
               State('mes', 'value'),
               State('ano', 'value')]
                      )
def run_jobs(_, contents, plataforma, empresa, mes, ano):
    if not contents:
        return [{}]

    if type(mes) != str:
        mes = mes['value']

    jobs = [s for s in jobs_c.run_job_now(contents, plataforma, empresa, mes, int(ano))]
    return jobs


@application.callback(Output('download-selected', 'href'),
                      [Input('job_table', 'selected_row_indices')],
                      [State('job_table', 'rows')])
def update_download_link(index, rows):
    df = pd.DataFrame.from_records(rows).iloc[index]
    csv_string = df.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8,"+ urllib.parse.quote(csv_string)
    return csv_string


app = application.server

if __name__ == '__main__':
    application.run_server(port=8080, debug=False, threaded=True, host='0.0.0.0')


