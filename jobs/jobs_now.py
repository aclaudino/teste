import pandas as pd
import numpy as np

from persistense.load_config import LoadConfig
from persistense.load_jobs import LoadJobs


def carrega_dataframe(file):
    df = pd.read_excel(file, na_values="", sheet_name=4, header=5)
    df.dropna(axis=0, how='all', inplace=True)
    df.replace(np.nan, '', regex=True, inplace=True)
    df.columns = [col.replace("\n", " ") for col in df.columns]
    return df


def valor_total_vendas(df):
    df["VALOR TOTAL VENDAS"] = df["PREÇO VENDA"] * df["QTDE VENDAS"]
    return df


def carrega_pracas(df, plataforma):
    platforms_conf = LoadConfig().platforms
    pracas = [p for u in platforms_conf.find({"_id": plataforma}) for p in u["PRAÇAS"]]
    return df.merge(pd.DataFrame(pracas))


def calcula_impostos(df):
    df["IMPOSTO"] = df["TAXA IMPOSTO"]*(df["VALOR TOTAL VENDAS"]/100)
    df["Líquido NET + EMPRESA"] = (100-df["TAXA IMPOSTO"])*(df["VALOR TOTAL VENDAS"]/100)
    return df


def calcula_share_plataforma(df, plataforma):
    platforms_conf = LoadConfig().platforms

    janelas = [p for u in platforms_conf.find({"_id": plataforma}) for p in u["JANELAS"]]
    df2 = df.merge(pd.DataFrame(janelas))

    df2["Líquido Empresa/Fornecedor"] = df2["Líquido NET + EMPRESA"] * (df2["SHARE"] / 100)

    return df2


def carrega_impostos_empresa_plataforma(df, plataforma, empresa):
    conf = LoadConfig()
    companies = next(conf.companies.find({"PLATAFORMA": plataforma, "EMPRESA": empresa}))
    df2 = df.copy()
    df2["TAXA EMPRESA"] = companies["TAXA"]
    df2["IMPOSTOS EMPRESA"] = df2["Líquido Empresa/Fornecedor"] * df2["TAXA EMPRESA"]/100.0
    return df2


def expande_plataforma(d, u):
    d["FILME"] = u["id"]

    return d


def carrega_contratos(df, plataforma):
    contracts = LoadConfig().contracts
    contratos = contracts.find({
        "$and": [
            {"PLATAFORMAS.PLATAFORMA": plataforma},
            {
                "$or": [{"PLATAFORMAS.id": u} for u in df["FILME"]]
            }
        ]
    })

    itens = [expande_plataforma(d, u)
             for d in contratos
             for u in d["PLATAFORMAS"]
             if u["PLATAFORMA"] == plataforma]

    new_columns = pd.DataFrame(itens).drop(columns=['PLATAFORMAS', '_id', 'id'])

    new_columns["PERCENTUAL FORNECEDOR"] = new_columns["PERCENTUAL FORNECEDOR"]*1.0

    return df.merge(new_columns)


def calcula_empresa_fornecedor_apos_impostos(df):
    df2 = df.copy()
    df2["Empresa+Fornecedor após impostos"] = df2["Líquido Empresa/Fornecedor"] - df2["IMPOSTOS EMPRESA"] - df2[
        "CONDECINE"]

    return df2


def calcula_parte_empresa_fornecedor(df):
    df2 = df.copy()
    df2["PARCELA FORNECEDOR"] = df2["Empresa+Fornecedor após impostos"] * df2["PERCENTUAL FORNECEDOR"]/100.0
    df2["PARCELA EMPRESA"] = df2["Empresa+Fornecedor após impostos"] * (1-df2["PERCENTUAL FORNECEDOR"]/100.0)
    return df2


def persist_result(df, plataforma, empresa, mes, ano):
    df = df.set_index(["FILME"])
    persist_to_platform(ano, df, empresa, mes, plataforma)
    persist_to_provider(ano, df, empresa, mes, plataforma)


def persist_to_provider(ano, df, empresa, mes, plataforma):
    for fornecedor, df_fornecedor in df.groupby(['FORNECEDOR']):
        d = {
            "plataforma": plataforma,
            "empresa": empresa,
            "mes": mes,
            "ano": ano,
            "fornecedor": fornecedor,
            "relatório": df_fornecedor.to_dict('Index')
        }
        LoadJobs().providers.insert_one(d)


def persist_to_platform(ano, df, empresa, mes, plataforma):
    d = {
        "plataforma": plataforma,
        "empresa": empresa,
        "mes": mes,
        "ano": ano,
        "sucesso": True,
        "relatório": df.to_dict('Index')
    }
    LoadJobs().platforms.insert_one(d)


def _expande_plataforma(d, u):
    d["FILME"] = u["_id"]

    return d
