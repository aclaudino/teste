class PipelineException(Exception):
    def __init__(self, message, errors):
        super(PipelineException, self).__init__(message)
        self.errors = errors