from setuptools import setup

setup(
    name='Job Server',
    include_package_data=True,
    install_requires=[
        'dash',
        'dash-renderer',
        'dash-html-components',
        'dash-core-components==0.13.0-rc5',
        'plotly',
        'pandas',
        'xlrd',
        'pymongo',
        'dash-table-experiments'
    ]
)

