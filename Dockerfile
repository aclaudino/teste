FROM python:3.6
MAINTAINER André Claudino "claudino@d2x.com.br"

ADD requirements.txt  /usr/src/app/
WORKDIR /usr/src/app/

ADD . /usr/src/app/

EXPOSE 8080
WORKDIR /usr/src/app

RUN apt-get update &&\
    echo "apt Atualizado" &&\
    apt-get install python3-pandas -y &&\
    echo "Pandas instalado" &&\
    apt-get clean

RUN pip3 install -e .

ENV PATH=$PATH:/usr/src/app/
ENV PYTHONPATH=/usr/src/app/
ENV FLASK_APP="/usr/src/app/main.py"

ENTRYPOINT ["flask", "run", "--port", "8080", "--host", "0.0.0.0"]