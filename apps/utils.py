import dash_table_experiments as dt
import dash_html_components as html
import dash_core_components as dcc
from persistense.load_config import LoadConfig

empresas = [
    "synapse brazil",
    "synapse produções",
    "aggregator llc",
    "aggregator corp"
]

MESES = [
    {'label': "Janeiro", 'value': "janeiro"},
    {'label': "Fevereiro", 'value': 'fevereiro'},
    {'label': "Março", 'value': 'marco'},
    {'label': "Abril", 'value': 'abril'},
    {'label': "Maio", 'value': 'maior'},
    {'label': "Junho", 'value': 'junho'},
    {'label': "Julho", 'value': 'julho'},
    {'label': "Agosto", 'value': 'agosto'},
    {'label': "Setembro", 'value': 'setembro'},
    {'label': "Outubro", 'value': 'outubro'},
    {'label': "Novembro", 'value': 'novembro'},
    {'label': "Dezembro", 'value': 'dezembro'}
]


def lista_plataformas():
    plataformas = [u["PLATAFORMA"]
        for u in
            LoadConfig()
            .companies
            .find()
            ]

    return [{
                'label': capitalize(u),
                'value': u
            } for u in set(plataformas)]


def capitalize(s):
    return " ".join([c[:1].upper() + c[1:] for c in s.split(" ")])


def map_empresas_to_drop_down():
    return [{'label': capitalize(s), 'value': s} for s in empresas]


def make_table(id, rows =[], **kwargs):
    return dt.DataTable(
                rows=rows,
                row_selectable=True,
                filterable=True,
                sortable=True,
                **kwargs,
                id=id)


def put_label(label, component):
    return html.Div([
        html.H3(label),
        component
    ])


def make_tab(tabs, tab_id, out_id, value=None):
    return\
        html.Div([
                    dcc.Tabs(
                        tabs=[{'label': l, 'value': v} for l, v in tabs],
                        value=value or tabs[0][1],
                        id=tab_id
                    ),
                    html.Div(id=out_id)
                ],
                style=
                {
                    'width': '100%',
                    'fontFamily': 'Sans-Serif',
                    'height': '20px'
                })

def desunderline(item):
    mapped = {}
    for key in item:
        mapped[key.replace("_", " ")] = item[key]
    return mapped

def lista_codigo_programa():
    codigo = [u["CODIGO DE PROGRAMA"]
                   for u in
                   LoadConfig().contracts.find()]

    return [{
        'label': capitalize(u),
        'value': u
    } for u in set(codigo)]