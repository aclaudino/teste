import dash_core_components as dcc
import dash_html_components as html
import dash_table_experiments as dt

_fake_table = html.Div(dt.DataTable(rows=[{}]), style={'display': 'none'})

layout = html.Div\
        (
            [
                dcc.Location(id='url', refresh=False),
                html.Div(id='page-content'),
                _fake_table
            ],
            id="content"
        )

