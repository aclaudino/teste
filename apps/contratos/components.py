from apps.utils import *

input_contrato=\
    html.Div(
        [
            dcc.Input(placeholder="Código de programa",
                      id='contract-codigo-de-programa',
                      style={'margin-left': '5px'},
                      type='text'),

            dcc.Input(placeholder="Fornecedor",
                      id='contract-fornecedor',
                      style={'margin-left': '5px'},
                      type='text'),

            dcc.Input(placeholder="Percentual Fornecedor",
                      id='contract-percentual-fornecedor',
                      style={'margin-left': '5px'},
                      type='text'),

            dcc.Input(placeholder="Codencine",
                      id='contract-codencine',
                      type='text',
                      style={'margin-left': '5px'}),

            dcc.Input(placeholder="Taxa",
                      id='contract-taxa',
                      type='text',
                      style={'margin-left': '5px'}),

            html.Div(id='contract-inserir-button',
                     className="glyphicon glyphicon-plus",
                     style={'margin-left': '10px',
                            'margin-top': '10px',
                            'color': 'blue'}),

            html.Div(id='contract-delete-button',
                     className="glyphicon glyphicon-trash",
                     style={'margin-left': '10px',
                            'margin-top': '10px',
                            'color': 'purple'})
        ],
        style={'width': '100%', 'margin-top': '20px', 'display': 'inline-block'})


input_contrato_plataformas =\
    html.Div([
        html.Div\
        (
            [
                dcc.Dropdown(id='input-codigo-de-programa-plataforma-contrato',
                             options=lista_codigo_programa(),
                             placeholder="Código de programa")
            ],
            style={'display': 'inline-block', 'width':'25%', 'margin-bottom': '-15px'}
        ),
        html.Div\
        (
            [
                dcc.Dropdown(id='input-plataforma-plataforma-contrato',
                             options=lista_plataformas(),
                             placeholder="Plataforma")
            ],
            style={'display': 'inline-block', 'width':'25%', 'margin-bottom': '-15px'}
        ),
        html.Div\
        (
            [
                dcc.Input(id='input-id-plataforma-contrato',
                           placeholder="id",
                           type="text")
            ],
            style={'display': 'inline-block', 'width':'25%'}
        ),

        html.Div(
            id='plataforma-contrato-inserir-button',
            className="glyphicon glyphicon-plus",
            style={'margin-left': '10px',
                   'margin-top': '10px',
                   'color': 'blue', 'display': 'inline-block'}
        ),
        html.Div(
            id='plataforma-contrato-delete-button',
            className="glyphicon glyphicon-trash",
            style={'margin-left': '10px',
                   'margin-top': '10px',
                   'color': 'purple',
                   'display': 'inline-block'}
        )

    ])

table_contratos_plataformas=\
    put_label('Plataformas Associadas',
              html.Div([
                input_contrato_plataformas,
                make_table(id='input-contrato-plataformas',
                         rows=[],
                         columns=["CODIGO DE PROGRAMA", "PLATAFORMA", "id"])
                ]))

