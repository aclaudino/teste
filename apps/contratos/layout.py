from .components import *


def parse(contract):
    return\
        {
            'CODIGO DE PROGRAMA': contract['CODIGO DE PROGRAMA'],
            'FORNECEDOR': contract['FORNECEDOR'],
            'PERCENTUAL FORNECEDOR': contract['PERCENTUAL FORNECEDOR'],
            'CONDECINE': contract['CONDECINE']
        }


contracts_config_table = make_table('contracts-config-table',
                                    [parse(_) for _ in LoadConfig().contracts.find()],
                                    columns=
                                        ["CODIGO DE PROGRAMA",
                                         "FORNECEDOR",
                                         "PERCENTUAL FORNECEDOR",
                                         "CONDECINE"]
                                    )

layout =\
    html.Div([input_contrato, contracts_config_table, table_contratos_plataformas])
