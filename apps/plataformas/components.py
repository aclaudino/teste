from ..utils import make_table
import dash_html_components as html
import dash_core_components as dcc

# Configuração de plataformas
pracas_config_table = \
    html.Div([
        html.Span([
            dcc.Input(placeholder="Praça", id='praca-input', type="text")
        ]),
        html.Span([
            dcc.Input(placeholder="Taxa Imposto", id='taxa-imposto-input', type="text")
        ]),
        html.Span(
                id='praca-inserir-button',
                className="glyphicon glyphicon-plus",
                style={'margin-left': '10px',
                        'color': 'blue',
                        'margin-bottom': '16px'}
        ),
        html.Span(
                id='praca-delete-button',
                className="glyphicon glyphicon-trash",
                style={'margin-left': '10px',
                        'color': 'purple',
                        'margin-bottom': '16px'}
                 ),
        html.Span(
                id='praca-save-button',
                className="glyphicon glyphicon-check",
                style={'margin-left': '10px',
                        'color': 'green',
                        'margin-bottom': '16px'}
                 ),
        make_table('plataformas-config-table', columns=["PRAÇA", "TAXA IMPOSTO"])
    ])

# Configuração de Janelas
janelas_config_table = \
    html.Div([
        html.Span([
            dcc.Input(placeholder="Janela", id='janela-input', type="text")
        ]),
        html.Span([
            dcc.Input(placeholder="Share", id='share-input', type="text")
        ]),
        html.Span(
                id='janela-inserir-button',
                className="glyphicon glyphicon-plus",
                style={'margin-left': '10px',
                        'color': 'blue',
                        'margin-bottom': '16px'}
        ),
        html.Span(
                id='janela-delete-button',
                className="glyphicon glyphicon-trash",
                style={'margin-left': '10px',
                        'color': 'purple',
                        'margin-bottom': '16px'}
                 ),
        html.Span(
                id='janela-save-button',
                className="glyphicon glyphicon-check",
                style={'margin-left': '10px',
                        'color': 'green',
                        'margin-bottom': '16px'}
                 ),
        make_table('janelas-config-table',
               columns=["JANELA", "SHARE"])
    ])
