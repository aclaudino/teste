from apps.utils import lista_plataformas
from .components import *
from ..utils import put_label, make_tab
import dash_html_components as html
import dash_core_components as dcc

pracas_layout =\
    put_label("Praça", pracas_config_table)

janelas_layout =\
    put_label("Janelas", janelas_config_table)

layout = put_label("Plataformas", html.Div([
            dcc.Dropdown(id='selected-platform', options=lista_plataformas(), placeholder="Plataforma"),
            make_tab([('Praças', 'pracas'), ('Janelas', 'janelas')],
             'tab-plataforma',
             'tab-plataforma-content')
        ]))