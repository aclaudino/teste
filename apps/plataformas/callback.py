from persistense.load_config import LoadConfig
import pandas as pd

def load_janelas(seleted_platform):
    if not seleted_platform:
        return [{}]

    return [janela
        for plataforma in
            LoadConfig().platforms.find({"_id": seleted_platform})
                for janela in plataforma['JANELAS']]


def load_pracas(seleted_platform):
    if not seleted_platform:
        return [{}]

    return [praca for plataforma in
            LoadConfig()
                .platforms
                .find({"_id": seleted_platform})
                    for praca in plataforma['PRAÇAS']]


def insert_praca(plataforma, nome, taxa, rows):
    try:
        plataforma = plataforma.lower()
        pracas = pd.DataFrame.from_records(rows)
        if nome in pracas["PRAÇA"].unique():
            # Se existir, atualiza
            pracas[pracas["PRAÇA"] == nome] = taxa
        else:
            # Se não, adiciona
            pracas = pracas.append({"PRAÇA": nome, "TAXA IMPOSTO": taxa}, ignore_index=True)
        records = pracas.to_dict("records")
        LoadConfig()\
            .platforms\
            .update({"_id": plataforma}, {"$set": {"PRAÇAS": records}})

    except:
        print("Error updating")
    finally:
        return plataforma