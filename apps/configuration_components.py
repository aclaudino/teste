from .utils import *
from bson.json_util import dumps


from persistense.load_config import LoadConfig

# Configuração de empresas
_empresas_config_table = \
    make_table('empresas-config-table', dumps(LoadConfig().platforms.find()))

empresas_layout =\
    put_label("Empresas", _empresas_config_table)

# Configuração de contratos
_contratos_config_table = \
    make_table('contratos-config-table', dumps(LoadConfig().platforms.find()))

contratos_layout =\
    put_label("Contratos", _contratos_config_table)