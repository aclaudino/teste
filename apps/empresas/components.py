import dash_core_components as dcc
import dash_html_components as html
from apps.utils import map_empresas_to_drop_down

input_empresa=\
    html.Div(
        [
            html.Div
            (
                [
                    dcc.Dropdown(
                        placeholder="Empresa",
                        id='empresa-nome-empresa',
                        clearable=True,
                        options=map_empresas_to_drop_down()
                    )
                ],
                style={'margin-left': '5px', 'width': '24%', 'display': 'inline-block', 'margin-bottom': '-16px'}
            ),

            html.Div(
                [
                dcc.Input(placeholder="Plataforma",
                          id='empresa-plataforma',
                          style={'margin-left': '5px', 'display': 'inline-block'},
                          type='text')
            ],
                style={'margin-left': '5px', 'width': '24%', 'display': 'inline-block'}
            ),

            html.Div
            (
                [
                    dcc.Input(placeholder="Taxa",
                              id='empresa-taxa',
                              style={'margin-left': '5px', 'display': 'inline-block'},
                              type='text')
                ],
                style={'margin-left': '5px', 'width': '24%', 'display': 'inline-block'}
            ),

            html.Div(
                    id='empresa-inserir-button',
                     className="glyphicon glyphicon-plus",
                     style={'margin-left': '10px',
                            'margin-top': '10px',
                            'color': 'blue', 'display': 'inline-block'}
            ),
            html.Div(
                    id='empresa-delete-button',
                     className="glyphicon glyphicon-trash",
                     style={'margin-left': '10px',
                            'margin-top': '10px',
                            'color': 'purple',
                            'display': 'inline-block'}
                     )
        ]
    )