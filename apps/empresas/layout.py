from ..utils import put_label, make_table
from .components import *
from persistense.load_config import LoadConfig


def parse(company):
    return\
        {
            'EMPRESA': company['EMPRESA'],
            'PLATAFORMA': company['PLATAFORMA'],
            'TAXA': company['TAXA']
        }


_empresas_config_table = make_table('empresas-config-table', [ parse(_) for _ in LoadConfig().companies.find()])


_empresas_base_layout = html.Div([input_empresa, _empresas_config_table])

layout =\
    put_label("Empresas", _empresas_base_layout)
