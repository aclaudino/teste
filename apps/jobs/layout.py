import dash_html_components as html
from apps.jobs import components as jc

empresa_plataforma = \
    html.Div([
                jc.plataforma_layout,
                jc.empresa_layout
            ],
            id="empresa-plataforma")

mes_ano =\
    html.Div([jc.mes_layout,
              jc.ano_layout,
              jc.uploader_layout,
              jc.executar_layout
             ],
             id="ano-mes")

layout = html.Div([empresa_plataforma, mes_ano, jc.table_layout])
