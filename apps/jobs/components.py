from datetime import datetime
from apps.utils import *

plataforma_layout =\
    html.Div([
        html.Label('Plataforma'),
        dcc.Dropdown(options=lista_plataformas(),
                     value='now', id='plataforma',
                     clearable=False)],
        style={'width': '49%', 'display': 'inline-block'})

empresa_layout =\
    html.Div([
        html.Label('Empresa'),
        dcc.Dropdown(options=map_empresas_to_drop_down(),
                     value="synapse brazil", id="empresa")],
        style={'width': '49%', 'float': 'right', 'display': 'inline-block'})

mes_layout = html.Div(
    [html.Label('Mês'), dcc.Dropdown(options=MESES,
                                     value=MESES[datetime.now().month - 1],
                                     id='mes',
                                     clearable=False)],
    style={'width': '24%', 'display': 'inline-block', 'float': 'left'})

ano_layout = html.Div(
    [html.Label('Ano'), html.Br(), dcc.Input(placeholder='Entre com o ano', type='number',
                                             id='ano',
                                             value=str(datetime.now().year))],
    style={'width': '24%', 'float': 'left', 'display': 'inline-block', 'padding-left': '10px', 'color': 'black'})

uploader_layout = html.Div([dcc.Upload([html.A('Planilha'),
                                        html.Div(id="filename")],
                                       style={'height': '30px', 'width': '100%', 'lineHeight': '30px',
                                              'borderWidth': '1px',
                                              'borderStyle': 'dashed', 'borderRadius': '5px', 'textAlign': 'center'},
                                       className="col-sm",
                                       id='uploader')],
                           style={'width': '15%', 'float': 'left', 'display': 'inline-block', 'padding-left': '5px',
                                  'padding-top': '25px'})

executar_layout = html.Div([html.Button('Executar', id='executar')],
                           style={'width': '15%', 'float': 'left', 'display': 'inline-block', 'padding-left': '5px',
                                  'padding-top': '25px'})


table_layout = html.Div([
            html.Div(id="message", className="alert alert-primary"),
            dt.DataTable(rows=[{}],
                row_selectable=True,
                filterable=True,
                sortable=True,
                selected_row_indices=[],
                id='job_table'),
            html.A("Download Selected", id="download-selected")
        ])
