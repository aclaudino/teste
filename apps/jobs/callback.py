from pipelines.pipeline_now import now_pipeline
import base64
import io


def run_job_now(contents, plataforma, empresa, mes, ano):
    content_type, content_string = contents.split(',')

    decoded = base64.b64decode(content_string)
    file = io.BytesIO(decoded)
    try:
        return now_pipeline(file, plataforma, empresa, mes, ano)
    except Exception as e:
        print(e)
