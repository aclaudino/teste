from pymongo import MongoClient
import os


class LoadConfig:

    def __init__(self):
        user = os.getenv('MONGO_CONFIG_USER', os.getenv('MONGO_USER', ""))
        password = os.getenv('MONGO_CONFIG_PASSWORD', os.getenv('MONGO_PASSWORD', ""))
        host = os.getenv('MONGO_CONFIG_HOST', os.getenv('MONGO_HOST', "localhost"))

        url = f"mongodb://{host}" if (user == "" or password == "") else f"mongodb://{user}:{password}@{host}"

        self.database = MongoClient(url).config

    @property
    def platforms(self):
        return self.database.platforms

    @property
    def contracts(self):
        return self.database.contracts

    @property
    def companies(self):
        return self.database.companies

