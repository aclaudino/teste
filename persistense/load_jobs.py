from pymongo import MongoClient
import os


class LoadJobs:

    def __init__(self):
        user = os.getenv('MONGO_JOB_USER', os.getenv('MONGO_USER', ""))
        password = os.getenv('MONGO_JOB_PASSWORD', os.getenv('MONGO_PASSWORD', ""))
        host = os.getenv('MONGO_JOB_HOST', os.getenv('MONGO_HOST', "localhost"))

        url = f"mongodb://{host}" if (user == "" or password == "") else f"mongodb://{user}:{password}@{host}"

        self.database = MongoClient(url).jobs

    @property
    def platforms(self):
        return self.database.platforms

    @property
    def providers(self):
        return self.database.providers

