from jobs.jobs_now import *


def now_pipeline(file, plataforma, empresa, mes, ano):
    df = carrega_dataframe(file)
    df = valor_total_vendas(df)
    df = carrega_pracas(df, plataforma)
    df = calcula_impostos(df)
    df = calcula_share_plataforma(df, plataforma)
    df = carrega_impostos_empresa_plataforma(df, plataforma, empresa)
    df = carrega_contratos(df, "now")
    df = calcula_empresa_fornecedor_apos_impostos(df)
    df = calcula_parte_empresa_fornecedor(df)

    persist_result(df, plataforma, empresa, mes, ano)
    return df.to_dict("records")
