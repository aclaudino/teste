import base64
import io

def parse_uploaded_excel(contents):
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)

    try:
        # Assume that the user uploaded an excel file
        return io.BytesIO(decoded)
    except Exception as e:
        print(e)
        return None